require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def target_mount_dir
    '/app'
  end

  def image_name
    ENV.fetch('TMP_IMAGE', 'kics:latest')
  end

  def privileged
    ENV.fetch('PRIVILEGED', 'true') == 'true'
  end

  context 'with no project' do
    let(:output) { `docker run -t --rm -w #{target_mount_dir} -e CI_PROJECT_DIR=#{target_mount_dir} #{image_name}` }
    let(:exit_code) { $CHILD_STATUS.to_i }

    it 'shows there is no match' do
      expect(output).to match(/no match in #{target_mount_dir}/i)
    end

    describe 'exit code' do
      specify { expect(exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context 'with test project' do
    def parse_expected_report(expectation_name)
      path = File.join(expectations_dir, expectation_name, 'gl-sast-report.json')
      JSON.parse(File.read(path))
    end

    def sorted_report(report)
      # Some reports are explicitly sorted to work around a Kics issue that produces different findings with
      # the same `cve` value.
      # See the description in https://gitlab.com/gitlab-org/security-products/analyzers/kics/-/merge_requests/62 for
      # more detail.
      (report['vulnerabilities'] || []).sort_by! do |v|
        # severity and cve are the default sort keys: https://gitlab.com/gitlab-org/gitlab/-/issues/383219.
        # description is added to account for a Kics rule that produces multiple findings with the same sort keys, but
        #   with a different description.
        # location.file is added to distinguish multiple findings with the same compare keys except for the filename.
        sort_keys = [v['severity'], v['cve'], v['description']]
        loc = v.dig('location', 'file')
        sort_keys << loc unless loc.nil?
        sort_keys
      end
      report
    end

    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',
        'CI_PROJECT_DIR': target_mount_dir,
        'SEARCH_MAX_DEPTH': 5  # the sources in some fixture directories are heavily nested
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        privileged: privileged,
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json')
    end

    let(:report) { scan.report }

    context 'with ansible' do
      let(:project) { 'ansible' }
      let(:report) { sorted_report(scan.report) }

      context 'by default' do
        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { sorted_report(parse_expected_report('ansible/default')) }
          end

          it_behaves_like 'valid report'
        end
      end

      context 'when including primary_identifiers' do
        let(:variables) do
          { 'GITLAB_FEATURES': 'sast_fp_reduction' }
        end

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { sorted_report(parse_expected_report('ansible/with-primary-identifiers')) }
          end

          it_behaves_like 'valid report'
        end
      end
    end

    context 'with cloudformation' do
      let(:project) { 'cloudformation' }

      context 'by default' do
        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { parse_expected_report('cloudformation/default') }
          end

          it_behaves_like 'valid report'
        end
      end

      context 'when using ruleset synthesis' do
        let(:project) { 'cloudformation-custom-ruleset' }

        it_behaves_like 'successful scan'

        let(:variables) do
          { 'GITLAB_FEATURES': 'sast_custom_rulesets' }
        end

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report(project)
            }
          end

          it_behaves_like 'valid report'
        end
      end

      context 'when including primary_identifiers' do
        let(:variables) do
          { 'GITLAB_FEATURES': 'sast_fp_reduction' }
        end

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { parse_expected_report('cloudformation/with-primary-identifiers') }
          end

          it_behaves_like 'valid report'
        end
      end
    end


    context 'with terraform' do
      let(:project) { 'terraform' }
      let(:report) { sorted_report(scan.report) }

      context 'by default' do
        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { sorted_report(parse_expected_report('terraform/default')) }
          end

          it_behaves_like 'valid report'
        end

      end

      context 'when including primary_identifiers' do
        let(:variables) do
          { 'GITLAB_FEATURES': 'sast_fp_reduction' }
        end

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { sorted_report(parse_expected_report('terraform/with-primary-identifiers')) }
          end

          it_behaves_like 'valid report'
        end
      end
    end
  end
end
