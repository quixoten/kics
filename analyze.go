package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/kics/metadata"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

const (
	outputPath = "/tmp"
	outputName = "kics"
)

// https://docs.kics.io/latest/results/#exit_status_code
var validExitStatuses = map[int]string{
	0:  "No Results were Found",
	50: "Found any HIGH Results",
	40: "Found any MEDIUM Results",
	30: "Found any LOW Results",
	20: "Found any INFO Results",
}

var defaultKicsQueryPath = path.Join("/", "usr", "local", "bin", "assets", "queries")

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, projectPath string) (io.ReadCloser, error) {
	// Load custom config if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	rulesetConfig, err := ruleset.Load(rulesetPath, metadata.AnalyzerID, log.StandardLogger())
	if err != nil {
		switch err.(type) {
		case *ruleset.NotEnabledError:
			log.Debug(err)
		case *ruleset.ConfigFileNotFoundError:
			log.Debug(err)
		case *ruleset.ConfigNotFoundError:
			log.Debug(err)
		case *ruleset.InvalidConfig:
			log.Fatal(err)
		default:
			return nil, err
		}
	}

	queryPath, err := getQueryPath(projectPath, rulesetConfig)
	if err != nil {
		return nil, err
	}

	reportPath := fmt.Sprintf("%s/%s.sarif", outputPath, outputName)

	log.Infof("path %s", projectPath)
	cmd := exec.Command("kics", buildArgs(projectPath, queryPath)...)
	log.Debug(cmd.String())

	scannerName := metadata.ReportScanner.Name
	output, err := cmd.CombinedOutput()
	if err != nil {
		exitErr, isExitErr := err.(*exec.ExitError)
		if !isExitErr {
			log.Errorf("Unknown error: %v, detail:\n%s", err, output)
			return nil, fmt.Errorf("%s scanner failure: %w", scannerName, err)
		}

		status, hasExitSignal := exitErr.Sys().(syscall.WaitStatus)
		if !hasExitSignal {
			log.Errorf("Couldn't get exit status; error: %v, detail:\n%s", exitErr, output)
			return nil, fmt.Errorf("%s scanner failure: %w", scannerName, exitErr)
		}

		exitStatus := status.ExitStatus()
		if _, ok := validExitStatuses[exitStatus]; !ok {
			log.Errorf("Encountered a system problem; status code: %d, error: %v, detail:\n%s",
				exitStatus, err, output)
			return nil, fmt.Errorf("%s scanner failure: %w", scannerName, err)
		}
	}

	log.Debugf("%s", output)

	return os.Open(reportPath)
}

// getQueryPath returns the path to available OPA queries
func getQueryPath(projectPath string, rulesetConfig *ruleset.Config) (string, error) {
	if rulesetConfig != nil && len(rulesetConfig.Passthrough) != 0 {
		targetDir, err := ruleset.ProcessPassthroughs(rulesetConfig, log.StandardLogger())
		if err != nil {
			return targetDir, err
		}
		if err := os.RemoveAll(defaultKicsQueryPath); err != nil {
			return "", err
		}
		if err := os.Rename(targetDir, defaultKicsQueryPath); err != nil {
			return "", err
		}
		// return targetDir, nil
	}

	return defaultKicsQueryPath, nil
}

func buildArgs(projectPath string, queryPath string) []string {
	return []string{
		"scan", "--ci",
		"--path", projectPath,
		"--queries-path", queryPath,
		// Disables request to https://kics.io to pull full descriptions
		"--disable-full-descriptions",
		"--disable-secrets",
		"--log-level", kicsLoglevel(),
		"--output-path", outputPath,
		"--output-name", outputName,
		"--report-formats", "sarif",
	}
}

// kicsLogLevel returns the uppercase log level equivalent
// as extracted from logrus, except for the "warning" level
// which is reformatted for compatibility with kics.
//
// See https://gitlab.com/gitlab-org/gitlab/-/issues/356920
func kicsLoglevel() string {
	logrusLevel := log.GetLevel()
	strLevel := logrusLevel.String()
	if logrusLevel == log.WarnLevel {
		strLevel = "warn"
	}

	return strings.ToUpper(strLevel)
}
