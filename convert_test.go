package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestConvert(t *testing.T) {
	tests := []struct {
		description string
		projectPath string
		reportPath  string
		wantReport  string
		in          string
	}{
		{
			description: "kics report from a terraform repo",
			reportPath:  "testdata/kics_reports/terraform.sarif",
			wantReport:  "testdata/expect/terraform/gl-sast-report.json",
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			f, err := os.Open(test.reportPath)
			defer f.Close()
			require.NoError(t, err)

			gotReport, err := convert(f, test.projectPath)
			require.NoError(t, err)

			var wantReport *report.Report
			bytes, err := ioutil.ReadFile(test.wantReport)
			require.NoError(t, err)

			err = json.Unmarshal(bytes, &wantReport)
			require.NoError(t, err)

			wantReport.DependencyFiles = []report.DependencyFile{}
			wantReport.Analyzer = "kics"
			wantReport.Config.Path = ".gitlab/sast-ruleset.toml"

			if diff := cmp.Diff(wantReport, gotReport); diff != "" {
				t.Errorf("Report mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

// TestComputeCompareKey ensures the generated `cve` value is stable for occurrences of the same vulnerability, and
// unique for different vulnerabilities.
func TestComputeCompareKey(t *testing.T) {
	v1 := report.Vulnerability{
		Identifiers: []report.Identifier{
			{
				Type:  "myIdentifierType",
				Value: "myIdentifierValue",
			},
		},
		Location: report.Location{
			LineStart: 10,
			LineEnd:   10,
		},
	}

	v2 := report.Vulnerability{
		Identifiers: []report.Identifier{
			{
				Type:  "myIdentifierType2",
				Value: "myIdentifierValue2",
			},
		},
		Location: report.Location{
			LineStart: 15,
			LineEnd:   15,
		},
	}

	assert.Equal(t, computeCompareKey(v1), computeCompareKey(v1), "same key for same vulnerability")
	assert.NotEqual(t, computeCompareKey(v1), computeCompareKey(v2), "different keys for different vulnerabilities")

	assert.Equal(t, computeCompareKey(v1), "myIdentifierType:myIdentifierValue:10:10")
	assert.Equal(t, computeCompareKey(v2), "myIdentifierType2:myIdentifierValue2:15:15")
}
